# Rclone

A container for mounting a Cloud folder with rclone

2020-01-24: Fix cache folders.
2020-02-14: Fix Environment variables for parameters.
